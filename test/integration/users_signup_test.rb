require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    log_in_as(users(:michael))
    get new_user_path
    assert_no_difference 'User.count' do
      post users_path, user: {name: "",
                              email: "user@invalid",
                              password: "foo",
                              password_confirmation: "bar" }
    end
    assert_template 'users/new'
  end

  test "valid signup information" do
    log_in_as(users(:michael))
    get new_user_path
    assert_difference 'User.count' do
      post_via_redirect users_path, user: { name: "Example User",
                                            email: "exampUser@example.com",
                                            password: "OUTfox11",
                                            password_confirmation: "OUTfox11" }
    end
    assert_template 'users/show'
    assert is_logged_in?
  end

end
