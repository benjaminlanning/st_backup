require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @user, user: {name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should allow admin to edit other users" do
    log_in_as(@user)
    get :edit, id: @other_user
    assert_template 'users/edit'
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@other_user)
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should allow admin to update other users" do
    log_in_as(@user)
    patch :update, id: @other_user, user: { name: @other_user.name, email: @other_user.email }
    assert_not flash.empty?
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'User.count' do
      post :create, user: {name: "Ben L",
                           email: "examplebenl@example.com",
                           password: "OUTfox11",
                           password_confirmation: "OUTfox11"}
    end
    assert_redirected_to login_url
  end

  test "should redirect create when logged in as non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      post :create, user: {name: "Ben L",
                           email: "examplebenl@example.com",
                           password: "OUTfox11",
                           password_confirmation: "OUTfox11"}
    end
    assert_redirected_to root_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as non-admin" do
    log_in_as(@other_user)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end

  test "should redirect make admin when not logged in" do
    patch :update, id: @other_user, user: { admin: true }
    assert_not @other_user.admin?
    assert_redirected_to login_url
  end

  test "should redirect make admin when logged in as non-admin" do
    log_in_as(@other_user)
    patch :update, id: @other_user, user: { admin: true }
    assert_not @other_user.admin?
    assert_redirected_to @other_user
  end

end