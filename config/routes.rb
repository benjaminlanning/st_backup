Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :users, only: [:index, :create, :show, :update, :destroy]
      resources :microposts, only: [:index, :create, :show, :update, :destroy]
    end
  end

  get 'sessions/new'

  root 'static_pages#home'

  get '/acb', :to => redirect("/acb.html")

  get 'about' => 'static_pages#about'
  get 'callback' => 'static_pages#callback'
  get 'new_user' => 'users#new'

  get 'dropbox' => 'dropbox#index'
  get 'dropbox_authorize' => 'dropbox#authorize'
  get 'dropbox_callback' => 'dropbox#dropbox_callback'
  get 'dropbox_deauthorize' => 'dropbox#deauthorize'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  resources :users
end