class User < ActiveRecord::Base
  attr_accessor :remember_token
  before_save :downcase_email

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  # Only accepts passwords between 6 and 32 characters in length
  PASSWORD_LENGTH_REGEX = /\A.{6,32}\z/
  # Only space and ASCII characters allowed in password
  PASSWORD_CHARACTERS_REGEX = /\A[\x20-\x7E]+\z/
  # Must have at least one uppercase character
  PASSWORD_UPPERCASE_REGEX = /\A.*[A-Z]+.*\z/
  # Must have at least one lowercase character
  PASSWORD_LOWERCASE_REGEX = /\A.*[a-z]+.*\z/
  # Must have at least one digit
  PASSWORD_DIGIT_REGEX = /\A.*[0-9]+.*\z/

  validates :name, presence: true,
                   length: { maximum: 50 }
  validates :email, presence: true,
                    length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, format: { with: PASSWORD_LENGTH_REGEX,
            message: "Passwords must be between 6 and 32 characters long." },
            allow_blank: true
  validates :password, format: { with: PASSWORD_CHARACTERS_REGEX,
            message: "Passwords can only ASCII characters and spaces." },
            allow_blank: true
  validates :password, format: { with: PASSWORD_UPPERCASE_REGEX,
            message: "Passwords must have at least one uppercase character." },
            allow_blank: true
  validates :password, format: { with: PASSWORD_LOWERCASE_REGEX,
            message: "Passwords must have at least one lowercase character." },
            allow_blank: true
  validates :password, format: { with: PASSWORD_DIGIT_REGEX,
            message: "Passwords must have at least one digit." },
            allow_blank: true

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  private

    def downcase_email
      self.email.downcase!
    end

end