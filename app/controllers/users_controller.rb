class UsersController < ApplicationController

  before_action :logged_in_user, only: [:create, :index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: [:create, :destroy]

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "User " + @user.name + " created successfully"
      redirect_to @user
    else
      render 'new'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def edit
    @user = User.find(params[:id])
  end

  def index
    @users = User.paginate(page: params[:page], :per_page => 10)
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def user_params
      if current_user && current_user.admin?
        params.require(:user).permit(:name, :email, :password,
                                     :password_confirmation, :admin)
      else
        params.require(:user).permit(:name, :email, :password,
                                     :password_confirmation, :dropbox_session)
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user) || current_user.admin?
    end

    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end