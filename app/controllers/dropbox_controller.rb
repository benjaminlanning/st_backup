class DropboxController < ApplicationController
  require 'dropbox_sdk'
  DROPBOX_APP_KEY = "og1ty1qap5jwkia"
  DROPBOX_APP_KEY_SECRET = "s6c7kzaw7oowgnz"

  def authorize
    dbsession = DropboxSession.new(DROPBOX_APP_KEY, DROPBOX_APP_KEY_SECRET)
    #serialize and save this DropboxSession
    session[:dropbox_session] = dbsession.serialize
    #pass to get_authorize_url a callback url that will return the user here
    redirect_to dbsession.get_authorize_url url_for(:action => 'dropbox_callback')
  end

  def deauthorize
    current_user.update_attributes(:dropbox_session => nil)
    flash[:success] = "You have successfully de-authorized from dropbox"

    redirect_to root_path
  end

  def dropbox_callback
    dbsession = DropboxSession.deserialize(session[:dropbox_session])
    dbsession.get_access_token #we've been authorized, so now request an access_token
    session[:dropbox_session] = dbsession.serialize
    current_user.update_attributes(:dropbox_session => session[:dropbox_session])
    session.delete :dropbox_session
    flash[:success] = "You have successfully authorized with dropbox."

    redirect_to dropbox_path
  end

  def index
    if current_user && current_user.dropbox_session
      dbsession = DropboxSession.deserialize(current_user.dropbox_session)
      client = DropboxClient.new(dbsession, "dropbox")

      customers = []
      client.metadata('/Don\'t Sync/Apps/AMC-STB/').each do |key,value|
        if key == "contents"
          value.each do |contentKey,contentValue|
            customers += [parseMD(contentKey, contentValue)]
          end
        end
      end

      customers.each do |cust|
        cust.contents = []
        client.metadata('/Don\'t Sync/Apps/AMC-STB/' + cust.name).each do |key,value|
          if key == "contents"
            value.each do |contentKey,contentValue|
              newBackup = parseMD(contentKey,contentValue)
              cust.contents += [newBackup]
            end
          end
        end
        cust.contents.sort! {|x,y| x.modified.to_date <=> y.modified.to_date}
        if !cust.contents.empty?
          cust.newest = cust.contents.last.modified
        else
          cust.newest = DateTime.new(1900,1,1)
        end
      end

      customers.each do |cust|
        if cust.newest > 7.days.ago
          cust.age = "week"
        elsif cust.newest > 30.days.ago
          cust.age = "month"
        elsif cust.newest > 90.days.ago
          cust.age = "quarter"
        elsif cust.newest > 365.days.ago
          cust.age = "year"
        else
          cust.age = "old"
        end
      end

      customers.sort! {|x,y| x.newest.to_date <=> y.newest.to_date}

      @data = customers

    else
      redirect_to dropbox_authorize_path
    end
  end

  private

    def parseMD(contentKey, contentValue)
      data = MD.new
      contentKey.each do |key,value|
        case key
          when "size"
            data.size = value
          when "bytes"
            data.bytes = value
          when "path"
            data.path = value
            data.name = File.basename(value)
          when "is_dir"
            data.is_dir = value
          when "is_deleted"
            data.is_deleted = value
          when "rev"
            data.rev = value
          when "hash"
            data.hash = value
          when "thumb_exists"
            data.thumb_exists = value
          when "photo_info"
            data.photo_info = value
          when "video_info"
            data.video_info = value
          when "icon"
            data.icon = value
          when "modified"
            data.modified = value
          when "client_mtime"
            data.client_mtime = value
          when "root"
            data.root = value
          when "shared_folder"
            data.shared_folder = value
          when "read_only"
            data.read_only = value
          when "parent_shared_folder_id"
            data.parent_shared_folder_id = value
          when "modifier"
            data.modifier = value
        end
      end

      return data
    end
end