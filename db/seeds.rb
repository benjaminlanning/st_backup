# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name: "Ben Lanning",
             email: "blanning@all-mode.com",
             password: "OUTfox11",
             password_confirmation: "OUTfox11",
             admin: true)

User.create!(name: "admin",
             email: "shoretel@all-mode.com",
             password: "OUTfox11",
             password_confirmation: "OUTfox11",
             admin: true)

User.create!(name: "Example user",
             email: "example@railstutorial.org",
             password: "OUTfox11",
             password_confirmation: "OUTfox11")

99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "OUTfox11"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password)
end