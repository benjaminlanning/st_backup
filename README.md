## README

ST (ShoreTel) Bakcup is a web page meant to be an internal dashboard for
[All-Mode Communications](http://www.all-mode.com) to monitor the status of system backups stored
in the AMC [Dropbox](http://www.dropbox.com).

Ruby version 4.2.5